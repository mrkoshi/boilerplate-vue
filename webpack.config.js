const webpack = require('webpack');
const path = require('path');

// Plugins
const { VueLoaderPlugin } = require('vue-loader');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const { NODE_ENV, PORT = 8080 } = process.env;
const IS_DEVELOPMENT = NODE_ENV === 'development';
const srcDir = path.join(__dirname, 'src');
const distDir = path.join(__dirname, 'dist');

module.exports = {
  context: srcDir,
  devtool: IS_DEVELOPMENT ? 'eval-sourcemap' : false,
  target: 'web',

  entry: path.join(srcDir, 'index'),
  output: {
    path: distDir,
    filename: 'js/[name].[hash:6].js',
    chunkFilename: 'js/[id].[name].[hash:6].js',
    publicPath: process.env.NODE_ENV === 'production'
      ? '/assets/'
      : '/'
  },

  devServer: {
    overlay: {
      warnings: false,
      errors: true
    },
    publicPath: `http://localhost:${PORT}`,
    contentBase: distDir,
    historyApiFallback: true,
    noInfo: true,
    port: PORT
  },
  watchOptions: {
    ignored: /node_modules/
  },

  module: {
    rules: [
      // SCRIPTS //
      {
        test: /\.(jsx?|vue)$/,
        enforce: 'pre',
        loader: 'eslint-loader',
        include: [srcDir]
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        include: [srcDir]
      },
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        include: [srcDir]
      },

      // HTML //
      {
        test: /\.pug$/,
        oneOf: [
          // this applies to <template lang="pug"> in Vue components
          {
            resourceQuery: /^\?vue/,
            use: ['pug-plain-loader']
          },
          // this applies to pug imports inside JavaScript
          {
            use: ['raw-loader', 'pug-plain-loader']
          }
        ]
      },
      {
        test: /\.html$/,
        loader: 'raw-loader'
      },

      // STYLES //
      {
        test: /\.(scss|css)$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                root: srcDir,
                importLoaders: true
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                plugins: [
                  require('postcss-import')(),
                  require('postcss-cssnext')({ warnForDuplicates: false }),
                  require('lost')(),
                  ...(IS_DEVELOPMENT ? [] : [require('cssnano')({ safe: true })])
                ]
              }
            },
            {
              loader: 'sass-loader',
              options: {
                includePaths: [
                  path.join(srcDir),
                  path.join(__dirname, 'node_modules')
                ]
              }
            }
          ]
        })
      },

      // ASSETS //
      {
        test: /\.(png|jpe?g|gif|webp|svg)$/i,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 5000,
            name: '[name].[hash:6].[ext]',
            outputPath: 'images/'
          }
        }, {
          loader: 'image-webpack-loader',
          options: {
            mozjpeg: {
              progressive: true,
              quality: 65
            },
            // optipng.enabled: false will disable optipng
            optipng: {
              enabled: false,
            },
            pngquant: {
              quality: '65-90',
              speed: 4
            },
            gifsicle: {
              interlaced: false,
            },
            // the webp option will enable WEBP
            webp: {
              quality: 75
            }
          }
        }]
      },
      {
        test: /\.(ttf|otf|woff|woff2)$/i,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[hash:6].[ext]',
            outputPath: 'fonts/'
          }
        }]
      },

      // MODERNIZR //
      {
        test: /\.modernizrrc$/,
        loader: ['modernizr-loader', 'json-loader']
      },
    ]
  },

  resolve: {
    extensions: ['.js', '.ts', '.vue'],
    modules: [srcDir, 'node_modules'],
    alias: {
      vue: path.join(__dirname, 'node_modules', 'vue', 'dist', 'vue.esm.js'),
      modernizr$: path.join(__dirname, '.modernizrrc')
    }
  },

  // PLUGINS //
  plugins: [
    new webpack.DefinePlugin({
      process: {
        env: {
          NODE_ENV: JSON.stringify(NODE_ENV)
        }
      }
    }),
    new VueLoaderPlugin(),
    new HtmlWebPackPlugin({
      template: path.join(srcDir, 'index.html'),
      minify: false
    }),
    new ExtractTextPlugin({
      filename: 'styles/[name].[hash:6].css',
      disable: IS_DEVELOPMENT
    })
  ]
};
