module.exports = {
  extends: [
    "airbnb-base",
    "plugin:vue/base"
  ],
  globals: {
    "NODE_ENV": true
  },
  env: {
    "browser": true,
    "node": true
  },
  rules: {
    "no-mixed-spaces-and-tabs": 1,
    "import/no-extraneous-dependencies": 0,
    "import/no-unresolved": 0,
    "import/extensions": 0,
    "import/prefer-default-export": 0,
    "class-methods-use-this": 0,
    "comma-dangle": ["error", "never"],
    "no-param-reassign": 0,
    "indent": [2, 2]
  },
  parserOptions: {
    "parser": "babel-eslint"
  },
  plugins: ["vue"]
};
