import axios from 'axios';
import qs from 'query-string';

export const api = axios.create({
  baseURL: process.env.NODE_ENV === 'development' ? 'http://test.ru/api' : '/api',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
});

api.interceptors.request.use(
  (config) => {
    const result = { ...config };

    if (config.headers['Content-Type'] === 'application/x-www-form-urlencoded') {
      result.data = qs.stringify(config.data);
    }

    return result;
  },
  reason => Promise.reject(reason)
);
