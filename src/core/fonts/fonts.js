import FontFaceObserver from 'font-face-observer';
import './fonts.scss';

const PrimaryFont = Promise.all(['normal', 500, 'bold'].map(weight => (
  new FontFaceObserver('Roboto', { weight }).check()
)));

PrimaryFont.then(() => {
  document.body.className += ' has-primary-font';
});
