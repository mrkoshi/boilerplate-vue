import 'babel-polyfill';
import Vue from 'vue';
import { Container } from 'components/container';
import { Header } from 'components/header';
import { Footer } from 'components/footer';

import './core/fonts';
import './index.scss';

Vue.component('w-container', Container);
Vue.component('w-header', Header);
Vue.component('w-footer', Footer);

export const app = new Vue({
  el: '#app'
});
