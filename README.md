# Boilerplate project

## Dependencies
- Webpack 4.20.2
- Vue 2.5.16

## Commands
- `yarn install`: Load and install packages from `package.json`
- `yarn run dev`: Starts webpack server for development
- `yarn run build`: Builds production version of the application

## Guidelines
- [Javascript](https://github.com/airbnb/javascript).
- [BEM Methodology](https://ru.bem.info/methodology/quick-start/).
- [SCSS](https://sass-guidelin.es/).

## Requirements
- Node.js >= 10.0.0
